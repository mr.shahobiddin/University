<html>
<head>
<title>Redirecting...</title>
<link rel="canonical" href="https://docs.gitlab.com/ce/university/high-availability/aws/"/>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="0; url=https://docs.gitlab.com/ce/university/high-availability/aws/" />
</head>
<body>
  <p><strong>Redirecting...</strong></p>
  <p><a href='https://docs.gitlab.com/ce/university/high-availability/aws/'>Click here if you are not redirected.</a></p>
  <script>
    document.location.href = "https://docs.gitlab.com/ce/university/high-availability/aws/";
  </script>
</body>
</html>
